package com.us.demo;

import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;

/**
 * * 
 * @author Sean
 * @since 2017-05-05
 * @version v 1.0
 * 
 * 应用 App Key 123456 /App Secret 000000
 * 签名 阿里大于
 * 短信模板 SMS_123
	
	模板类型: 短信通知
	模板名称: 时间通知
	模板ID: SMS_37000000
	模板内容: 尊敬的${name}，您于${time}，特此短信告知。
	申请说明: 告知消息的时间。 
 * */
public class Test {
	public static void main(String[] args) throws ApiException, InterruptedException {
		for (int i = 1; i <= 1; i++) {
			String url = "http://gw.api.taobao.com/router/rest";
			String appkey = "123456";
			String secret = "000000";
			TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
			AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
			req.setExtend("");
			req.setSmsType("normal");
			req.setSmsFreeSignName("阿里大于");
			req.setSmsParamString(
					"{name:'HelloMan',time:'1209113900'}");
			req.setSmsTemplateCode("SMS_37000000");
			AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
			Thread.sleep(1000);
			System.out.println(rsp.getBody());
		}
	}

}
