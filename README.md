# SMS 短信发送API开发介绍
* 阿里大于注册
* 阿里大于应用/模板/签名 介绍
* 阿里大于API介绍
* 阿里大于Demo

## 1.1 阿里大于注册

阿里大于的帐户可以使用阿里的帐号登陆，使用账户前需要进行实名制的认证。[阿里大于地址](http://www.alidayu.com)。

## 1.2 阿里大于应用/模板/签名 介绍

在获取发送短信的Demo前，我们应当查看如下几点内容:
* 应用 应用拥有一对(App Key/App Secret)
* 签名 短语发送的时候一般会有一个标识人的名称
* 模板 阿里为了使短信内容的规范化，特别加了一个模板的限制.（工信部的要求）

## 1.3 阿里大于API介绍
具体的细节可以查看这里[阿里大于短信API文档](https://api.alidayu.com/doc2/apiDetail.htm?spm=a3142.8062996.3.1.0LSXRc&apiId=25450)，这边只是简单的介绍一下。

###alibaba.aliqin.fc.sms.num.send (短信发送)

着重介绍一下方法的请求参数。

| 名称     | 类型  |是否必须示例值 |更多限制|描述|
| --------|:-----:|:-----:|:-----:|:----- |
| extend  | String| 可选| 123456|公共回传参数，在“消息返回”中会透传回该参数；举例：用户可以传入自己下级的会员ID，在消息返回时，该会员ID会包含在内，用户可以根据该会员ID识别是哪位会员使用了你的应用|
| sms_type | String| 必须| normal |短信类型，传入值请填写normal |
| sms_free_sign_name | String| 必须 | 阿里大于|短信签名，传入的短信签名必须是在阿里大于“管理中心-验证码/短信通知/推广短信-配置短信签名”中的可用签名。如“阿里大于”已在短信签名管理中通过审核，则可传入”阿里大于“（传参时去掉引号）作为短信签名。短信效果示例：【阿里大于】欢迎使用阿里大于服务。|
|sms_param | Json| 可选| {"code":"1234","product":"alidayu"} |短信模板变量，传参规则{"key":"value"}，key的名字须和申请模板中的变量名一致，多个变量之间以逗号隔开。示例：针对模板“验证码${code}，您正在进行${product}身份验证，打死不要告诉别人哦！”，传参时需传入{"code":"1234","product":"alidayu"}|
|  rec_num | String| 必须| 13000000000 |短信接收号码。支持单个或多个手机号码，传入号码为11位手机号码，不能加0或+86。群发短信需传入多个号码，以英文逗号分隔，一次调用最多传入200个号码。示例：18600000000,13911111111,13322222222 |
| sms_template_code | String| 必须| SMS_585014 |短信模板ID，传入的模板必须是在阿里大于“管理中心-短信模板管理”中的可用模板。示例：SMS_585014 |

## 1.4 阿里大于Demo
### 1.4.1 SDK下载

下载阿里大于提供的SDK包[点击下载](http://download.taobaocdn.com/freedom/38879/compress/sdk-java-2016-06-07.zip?spm=a3142.8062996.3.d6000.0LSXRc&file=sdk-java-2016-06-07.zip)，并将其加入到我们项目的ClassPath目录下。

### 1.4.2 数据准备

下面我们在写Demo的时候，默认我们已经获取了阿里大于应用的几个组件(应用/模板/签名)。
下方的几个配置部分皆为虚构，读者可自行替换成自己的配置。

* 应用 App Key 123456 /App Secret 000000
* 签名 阿里大于
* 短信模板 SMS_123
 
 ```
模板类型: 短信通知
模板名称: 时间通知
模板ID: SMS_37000000
模板内容: 尊敬的${name}，您于${time}，特此短信告知。
申请说明: 告知消息的时间。 
 ```

### 1.4.3 示例代码
示例代码如下:

```
package com.us.demo;

//import the jars 
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;

public class main {
	public static void main(String []args) throws ApiException, InterruptedException{
		for (int i = 1; i <= 1; i++) {
			String url = "http://gw.api.taobao.com/router/rest";
			String appkey = "123456"; 
			String secret = "000000";
			TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
			AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
			req.setExtend("");
			req.setSmsType("normal");
			req.setSmsFreeSignName("阿里大于");
			req.setSmsParamString("{name:'HelloMan',time:'1209113900'}");
			req.setRecNum("13000000000"); //发送人 可以为电话List
			req.setSmsTemplateCode("SMS_123");//模板的ID
			AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
			Thread.sleep(1000);
			System.out.println(rsp.getBody());
		}
	}
}
```
### 1.4.4 代码地址
 [示例代码地址](http://git.oschina.net/yanxml/smsdemo)

### 2. 参考地址
[1] [官方API参考文档](https://api.alidayu.com/doc2/apiDetail.htm?spm=a3142.8062996.3.1.0LSXRc&apiId=25450)

